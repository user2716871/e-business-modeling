#include "block.h"

using namespace std;

ostream& operator<<(ostream& os, const BlockState& state) {

	if (state == BlockState::Sleep) {
		os << 0;
	}
	else {
		os << 1;
	}
	return os;
}


Block::Block(string name, int work_time_expected, int work_time_deviation) :
	name_(move(name)),
	work_time_generator_(work_time_expected, work_time_deviation),
	inputs_count_(0)
{
	Refresh();
}

int Block::GetWorkTime() {

	while (true) {
		int result = round(work_time_generator_.Get());
		if (result > 0) {
			return result;
		}
	}
}

void Block::Link(Block& destination) {
	outputs_.push_back(&destination);
	++destination.inputs_count_;
}

void Block::Refresh() {
	active_inputs_counter_ = 0;
	state_ = BlockState::Sleep;
}
