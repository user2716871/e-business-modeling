#include <iostream>
#include <string>

#include "block.h"
#include "model.h"

using namespace std;

int main() {

	Model model(GetBlocks(), 160, 100, 0.02);

	model.Simulate();
}

