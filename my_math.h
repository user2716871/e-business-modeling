#pragma once

#include <vector>

const double PI = 3.14159265359;

double Sqr(double x);

double GetExpectedValue(const std::vector<double>& values);

double GetDeviationValue(const std::vector<double>& values);
