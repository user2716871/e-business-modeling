#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>

#include <algorithm>

#include "block.h"

std::vector<Block*> GetBlocks();

struct Model {

	Model(
		std::vector<Block*>&& blocks, 
		int max_time, 
		int boards_per_cycle, 
		double defect_prob
	);

	void Simulate();

	void GetStates(int t);

	void UpdateStates();

	void UpdateTimers();

	void PrintToFile();

	std::vector<Block*> blocks_;
	std::vector<std::vector<BlockState>> state_by_second_and_block_id_;
	std::vector<int> counter_for_block_;

	const int max_time_;
	const int boards_per_cycle_;
	const double defect_prob_;
};
